### How to use this spring-boot project

- Run the app by importing this using below command in your command shell
  
  `git clone https://NatsuZeref@bitbucket.org/NatsuZeref/java-challenge.git` 
- Swagger UI : http://localhost:8080/swagger-ui.html
- H2 UI : http://localhost:8080/h2-console

> Don't forget to set the `JDBC URL` value as `jdbc:h2:mem:testdb` for H2 UI.


#### features
1. SLf4j Log implementation
2. Exception handling added
3. Mockito Tests added.
4. Java Docs added.

#### Improvements if there was more time.
1. Redis Cache Implementation instead of Spring Cache.
2. Dedicated Response for Error Scenarios.


#### Runtime
- java 8

#### Experience in Java
- I know Spring Boot very well and have been using it for 6 years .
