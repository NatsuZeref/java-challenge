package jp.co.axa.crudapis.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

import jp.co.axa.crudapis.entities.Employee;
import jp.co.axa.crudapis.exception.EmployeeNotFoundException;
import jp.co.axa.crudapis.repositories.EmployeeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	/** Logger instance */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	/** Repository instance */ 
	@Autowired
	private EmployeeRepository employeeRepository;

	 /**
     * Retrieve all employee 
     *
     * @return All employee data
     * @throws EmployeeNotFoundException
     */
	@Cacheable(cacheNames = "allEmployeesCache")
	public List<Employee> retrieveEmployees() {
		List<Employee> employees = employeeRepository.findAll();
		if (employees.size() == 0) {
			LOGGER.info("No records found in Database");
		}
		return employees;
	}

	 /**
     * Get Employee 
     *
     * @param employeeID Employee Id
     * @return Employee data
     * @throws EmployeeNotFoundException
     */
	@Cacheable(value = "employeeCache", key = "#employeeId")
	public Employee getEmployee(Long employeeId) {
		Optional<Employee> optEmp = employeeRepository.findById(employeeId);
		if (!optEmp.isPresent()) {
			LOGGER.error("Not found Employee of employee id {}", employeeId); 
			throw new EmployeeNotFoundException(employeeId);
		}
		return optEmp.get();
	}

	 /**
     * Save Employee 
     *
     * @param employee Employee
     * @return void
     */
	@Caching(evict = { 
			@CacheEvict(value = "allEmployeesCache", allEntries = true),
			@CacheEvict(value = "employeeCache", key = "#employee.id") 
			}
	)
	public void saveEmployee(Employee employee) {
		employeeRepository.save(employee);
	}

	 /**
     * Delete employee ID
     *
     * @param employeeID Employee Id
     * @return void
     * @throws EmployeeNotFoundException
     */
	@Caching(evict = { 
			@CacheEvict(value = "allEmployeesCache", allEntries = true),
			@CacheEvict(value = "employeeCache", key = "#employeeId") 
			}
	)
	public void deleteEmployee(Long employeeId) {
		getEmployee(employeeId);
		employeeRepository.deleteById(employeeId);
	}

	 /**
     * Update Process
     *
     * @param updateData New employee data
     * @param employeeID Employee Id
     * @return void
     * @throws EmployeeNotFoundException
     */
	@Caching(evict = { 
			@CacheEvict(value = "allEmployeesCache", allEntries = true),
			@CacheEvict(value = "employeeCache", key = "#employeeID") 
			}
	)
	public void updateEmployee(Employee updateData, Long employeeID) {
		Optional<Employee> employee = employeeRepository.findById(employeeID);
		if (!employee.isPresent()) {
			throw new EmployeeNotFoundException(employeeID);
		} else {
			employee.get().setName(updateData.getName());
			employee.get().setDepartment(updateData.getDepartment());
			employee.get().setSalary(updateData.getSalary());
			employeeRepository.save(employee.get());
		}
	}

}