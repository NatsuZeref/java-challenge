package jp.co.axa.crudapis.services;

import java.util.List;

import jp.co.axa.crudapis.entities.Employee;

public interface EmployeeService {

    public List<Employee> retrieveEmployees();

    public Employee getEmployee(Long employeeId);

    public void saveEmployee(Employee employee);

    public void deleteEmployee(Long employeeId);

    public void updateEmployee(Employee employee, Long id);
}