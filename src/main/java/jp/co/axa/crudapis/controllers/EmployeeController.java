package jp.co.axa.crudapis.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jp.co.axa.crudapis.entities.Employee;
import jp.co.axa.crudapis.exception.EmployeeNotFoundException;
import jp.co.axa.crudapis.services.EmployeeService;

import java.util.List;


/**
 * REST Controller Class<BR />
 * CRUD Operation processor
 *
 * @author Sunil.Nishith
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

	/** Logger Instance */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	/** Service instance **/ 
    @Autowired
    private EmployeeService employeeService;

    /**
     * Retrieve all employees<BR />
     *
     * @param employeeId Employee ID
     * @return all employee data 
     */
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employees = employeeService.retrieveEmployees();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
    
    /**
     * GET Employees<BR />
     *
     * @param employeeId Employee ID
     * @return Employee
     */
    @GetMapping("/employees/{employeeId}")
    public Employee getEmployee(@PathVariable(name="employeeId")Long employeeId) {
        return employeeService.getEmployee(employeeId);
    }

    /**
     * Save Employee<BR />
     *
     * @param employee Employee Details
     * @return void
     */
    @PostMapping("/employees/save")
    public void newEmployee(@RequestBody Employee employee){
        employeeService.saveEmployee(employee);       
    }

    /**
     * Delete Employee<BR />
     *
     * @param employeeId Employee ID 
     * @return void
     */
    @DeleteMapping("/employees/delete/{employeeId}")
    public void deleteEmployee(@PathVariable(name="employeeId")Long employeeId){
        employeeService.deleteEmployee(employeeId);
        LOGGER.info("deleted the employee with employee id {}", employeeId);
        
    }

    /**
     * Update Employee data<BR />
     *
     * @param Employee Employee data 
     * @param employeeId Employee ID 
     * @return void
     */
    @PutMapping("/employees/update/{employeeId}")
    public void updateEmployee(@RequestBody Employee employee,
                               @PathVariable(name="employeeId")Long employeeId) throws EmployeeNotFoundException{
        employeeService.updateEmployee(employee, employeeId);
        LOGGER.info("Updated the employee with employee id {}", employeeId);        
    }

}
