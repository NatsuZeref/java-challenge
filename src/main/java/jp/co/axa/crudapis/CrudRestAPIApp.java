package jp.co.axa.crudapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@EnableCaching
@Slf4j
public class CrudRestAPIApp {

	public static void main(String[] args) {
		SpringApplication.run(CrudRestAPIApp.class, args);
	}

}
