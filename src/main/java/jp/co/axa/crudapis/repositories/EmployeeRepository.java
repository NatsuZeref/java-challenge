package jp.co.axa.crudapis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.co.axa.crudapis.entities.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}

