package jp.co.axa.crudapis.service;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import jp.co.axa.crudapis.entities.Employee;
import jp.co.axa.crudapis.exception.EmployeeNotFoundException;
import jp.co.axa.crudapis.repositories.EmployeeRepository;
import jp.co.axa.crudapis.services.EmployeeServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {
	
	@Mock
	private EmployeeRepository repository;
	
	@InjectMocks
	private EmployeeServiceImpl service;
	
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

	@Test
	public void whenFindAllEmployees_testIfFindAllisInvoked() {
		when(repository.findAll()).thenReturn(new ArrayList<Employee>());
		service.retrieveEmployees();
		verify(repository, times(1)).findAll();		
	}
	
	@Test
	public void whenGetEmployee_testFindByIDIsInvoked() {
		Long employeeID = 1L;
		Employee employee = new Employee(employeeID, "nishith", Integer.valueOf(10000000), "CS");
		when(repository.findById(employeeID)).thenReturn(Optional.of(employee));
		service.getEmployee(employeeID);
		verify(repository, times(1)).findById(employeeID);
	}
	
	
	@Test(expected = EmployeeNotFoundException.class)
	public void whenGetEmployeeNotPresentInDB_thenThrowEmployeeNotFoundException() {
		Long employeeID = 1L;
		Employee employee = null;
		when(repository.findById(employeeID)).thenReturn(Optional.empty());
		service.getEmployee(employeeID);
	}
	
	@Test
	public void whenSaveEmployee_testIfSaveIsInvoked() {
		Employee employee = new Employee(1L, "nishith", Integer.valueOf(10000000), "CS");
		when(repository.save(employee)).thenReturn(employee);
		service.saveEmployee(employee);
		verify(repository, times(1)).save(employee);
	}

	@Test
	public void whenUpdateEmployee_TestIfSaveisInvoked() {
		Long employeeID = 1L;
		Employee employee = new Employee(1L, "nishith", Integer.valueOf(10000000), "CS");
		when(repository.findById(employeeID)).thenReturn(Optional.of(employee));
		when(repository.save(employee)).thenReturn(employee);
		service.updateEmployee(employee, employeeID);
		verify(repository, times(1)).save(employee);	
		
	}
	
	@Test(expected = EmployeeNotFoundException.class)
	public void whenUpdateEmployeeNotPresentInDB_thenThrowEmployeeNotFoundException() {
		Long employeeID_NotInDB = 100L; 
		Employee employeeRetrievedFromClient = new Employee(employeeID_NotInDB, "nishith", Integer.valueOf(10000000), "CS");
		when(repository.findById(employeeID_NotInDB)).thenReturn(Optional.empty());
		service.updateEmployee(employeeRetrievedFromClient, employeeID_NotInDB);	
	}
	
	@Test
	public void whenDeleteEmployeePresentInIDB_testIfDeleteByIDisInvoked() {
		Long employeeID = 1L;
		Employee employee = new Employee(1L, "nishith", Integer.valueOf(10000000), "CS");
		when(repository.findById(employeeID)).thenReturn(Optional.of(employee));
		doNothing().when(repository).deleteById(employeeID);
		service.deleteEmployee(employeeID);
		verify(repository, times(1)).deleteById(employeeID);	
	}
	
	@Test(expected = EmployeeNotFoundException.class)
	public void whenDeleteEmployeePresentNotPresentInIDB_thenThrowEmployeeNotFoundException() {
		Long employeeID_NotInDB = 100L;		
		when(repository.findById(employeeID_NotInDB)).thenReturn(Optional.empty());
		service.deleteEmployee(employeeID_NotInDB);
		verify(repository, times(0)).deleteById(employeeID_NotInDB);				
	}
	
}
